<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title>Documents Module</title>
<link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
</head>
<body>
<div class="container" style="width:700px;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <form class="form-inline" method="GET" action="/">
                <div class="form-group">
                    <label for="word">Search:</label>
                    <input id="word" required="true" name="word" 
                    value="{{\Illuminate\Support\Facades\Input::get('word',false)}}" 
                    class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i> Search</button>
            </form>
        </div>
	</div>
</div>
<div class="container" style="width:700px;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <form class="form-inline" method="POST" action="/document/save">
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input required="true" id="title" name="title" class="form-control">
                </div>
                <div class="form-group">
                    <label for="desc">Description:</label>
                    <input required="true" id="desc" name="desc" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add</button>
            </form>
        </div>
    </div>
    @foreach($documents as $d)
        <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-2x fa-file-pdf-o" aria-hidden="true"></i> {{$d->title}}
                <button type="button" class="btn-edit btn btn-warning pull-right">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                </button></div>
            <div class="panel-heading edit-form" style="display:none">
                <form class="form-inline" method="POST" action="/document/save/{{$d->id}}">
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input id="title" name="title" class="form-control" value="{{$d->title}}">
                    </div>
                    <div class="form-group">
                        <label for="desc">Description:</label>
                        <input id="desc" name="desc" class="form-control" value="{{$d->description}}">
                    </div>
                    <button type="submit" class="btn btn-info">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Submit</button>
                </form>
            </div>
            <div class="panel-body">
                <div class="col-lg-6">
                    <h5 class="help-block">{{$d->description}}</h5>
                    <hr/>
                    <form action="/email/{{$d->id}}" method="POST">
                        <div class="form-group">
                            <label for="mail_to">Email to:</label>
                            <input id="mail_to" required="true" name="to" placeholder="example@gmail.com"/>
                        </div>
                        <button type="submit" class="btn btn-default">Send Email</button>
                    </form>
                </div>
                <div class="col-lg-6">
                    <ul class="list-group">
                        @foreach($d->versions as $v)
                        <li class="list-group-item">Version {{$v->version}} {{$v->file_size}}KB
                            <a href="/documents/{{$v->file_path}}" target="_blank"> link </a>
                        </li>
                        @endforeach
                        <li class="list-group-item">
                            <form class="form-inline" method="POST" enctype="multipart/form-data" action="/document_version/save/{{$d->id}}">
                                <input type="file" name="document">
                                <input type="submit" value="Submit" class="btn btn-info">
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    @endforeach
    @if(\Illuminate\Support\Facades\Input::get('word',false) && $documents->isEmpty())
        No records found
    @endif

</div>
<script type="text/javascript">
    window.onload = function(){
        var edit_buttons = document.getElementsByClassName("btn-edit");
        for(var i in edit_buttons){
            var btn = edit_buttons[i];
            btn.onclick = function(){
                this.parentElement.parentElement.getElementsByClassName("edit-form")[0].style.display = "Block";
                this.parentElement.style.display = "None";
            }
        }
    }
</script>
</body>

</html>


