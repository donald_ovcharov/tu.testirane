<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use \App\Document;
use \App\DocumentVersion;
use \Illuminate\Support\Facades\Input;
use Log;

$store = function($id = false){
    if(!$id){
        $document = new Document();
    }else{
        $document = Document::find($id);
    }
    $document->title = Input::get('title');
    $document->description = Input::get('desc');
    $document->save();
    return redirect('/');
};

$store_version = function($document_id,\Illuminate\Http\Request $request){
    /** @var Document $document */
    $document = Document::with('versions')->find($document_id);
    $file = $request->file('document');
    $document_version = new DocumentVersion();
    $document_version->version = $document->versions->count() + 1;
    $document_version->file_size = round($file->getSize()/1000,2);
    $document_version->file_type = $file->getClientOriginalExtension();
    $document_version->file_path = $document_id.'-'.$document_version->version.'.'.$document_version->file_type;
    $file->move('documents', $document_version->file_path);
    $document->versions()->save($document_version);
    return redirect('/');
};

$email = function($document_id){
    $document_version = DocumentVersion::
        with('origin')
        ->orderBy('created_at','desc')
        ->where('document_id',$document_id)
        ->first();
    $email_to = Input::get('to');
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 1;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'tls';
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587; // or 587
    $mail->IsHTML(true);
    $mail->Username = env('GMAIL_USER');
    $mail->Password = env('GMAIL_PASS');
    $mail->addAddress($email_to);
    $mail->setFrom('testirane@example.com');
    $mail->Subject = "Sending document ".$document_version->origin->title. " version ".$document_version->version;
    $mail->Body = "<a href='http://testirane.app/documents/{$document_version->file_path}'>link</a>";
    $mail->addAttachment(base_path('public/documents/'.$document_version->file_path),
    $document_version->origin->title.'-v'.$document_version->version.'.'.$document_version->file_type);
    if(!$mail->Send()) {
        Log::info("Mailer Error: ".$mail->ErrorInfo);
        } else {
        Log::info("Message sent!");
        }
    return redirect('/');
};

$index = function () {
    $documents = Document::with('versions');
    if($search = Input::get('word',false)){
        $documents->where('title','like',"%$search%");
    }
    $documents = $documents->orderBy('created_at','desc')->get();

    return view('index')->with(['documents'=>$documents]);
};


$app->get('/', $index);
$app->post('/document/save',$store );
$app->post('/document/save/{id}',$store );
$app->post('/document_version/save/{document_id}',$store_version);
$app->post('/email/{document_id}',$email);

