<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model{
    public function versions(){
        return $this->hasMany('\App\DocumentVersion','document_id','id');
    }
}