<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentVersion extends Model{
    protected $table = 'documents_versions';

    public function origin(){
        return $this->belongsTo('\App\Document','document_id','id');
    }
}