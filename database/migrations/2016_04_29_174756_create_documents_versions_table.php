<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_versions',function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('document_id');
            $table->float('version');
            $table->string('file_type');
            $table->string('file_path');
            $table->float('file_size');
            $table->timestamps();
            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents_versions');
    }
}
