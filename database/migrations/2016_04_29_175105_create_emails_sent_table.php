<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsSentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails_sent',function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('document_version_id');
            $table->foreign('document_version_id')->references('id')->on('documents_versions');
            $table->string('sent_to');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emails_sent');
    }
}
